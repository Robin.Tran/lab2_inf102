package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() { //arraylist alltid fast lengde, og hvis den er full så må du lagekopi
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	@SuppressWarnings ("unchecked") //har noe med typer å gjøre
	public T get(int index) {
		//Det vi gir get er for å sjekke hvilken element som er på den indeksen, eks hva er på indeks 3
		if (index < 0 || index >= n) {
			throw new IndexOutOfBoundsException("Index out of bounds");
		}
		return (T) elements[index]; //generiske type
	}
	
	@Override
	public void add(int index, T element) {
		if (index < 0 || index > n) {
			throw new IndexOutOfBoundsException("Index out of bounds");
		}
		if (elements.length == n) { //sjekker om elements lengden er like lang som str på listen
			int newArraylist = elements.length * 2; //lager en ny arraylist ved å gange på 2
			elements = Arrays.copyOf(elements, newArraylist); //kopierer elements i nye arrayList
		}
		for (int i = n; i > index; i--) {
			elements[i] = elements[i - 1];
		}
		elements[index] = element;
		n++;

		return;

	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}